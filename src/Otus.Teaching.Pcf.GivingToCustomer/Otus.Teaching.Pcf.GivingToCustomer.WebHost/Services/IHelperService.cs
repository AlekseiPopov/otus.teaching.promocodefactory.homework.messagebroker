﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.Pcf.GivingToCustomer.WebHost.Models;

namespace Otus.Teaching.Pcf.GivingToCustomer.WebHost.Services
{
    public interface IHelperService
    {
        public Task<bool> GivePromoCodesToCustomersWithPreferenceAsync(GivePromoCodeRequest request);
    }
}
