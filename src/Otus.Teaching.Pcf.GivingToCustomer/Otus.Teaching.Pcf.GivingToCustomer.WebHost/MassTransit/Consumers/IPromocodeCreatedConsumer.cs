﻿using System;
using System.Threading.Tasks;
using GreenPipes;
using MassTransit.ConsumeConfigurators;
using MassTransit.Contracts;
using MassTransit.Definition;
using Otus.Teaching.Pcf.GivingToCustomer.WebHost.Models;
using Otus.Teaching.Pcf.GivingToCustomer.WebHost.Services;
using Otus.Teaching.Pcf.ReceivingFromPartner.WebHost.MassTransit.Constans;

namespace MassTransit.Consumers
{
    public class IPromocodeCreatedConsumer : IConsumer<IPromocodeCreated>
    {
        private readonly IHelperService _helperService;

        public IPromocodeCreatedConsumer(IHelperService helperService)
        {
            _helperService = helperService;
        }
        public async Task Consume(ConsumeContext<IPromocodeCreated> context)
        {
            GivePromoCodeRequest request = new GivePromoCodeRequest
            {
                ServiceInfo = context.Message.ServiceInfo,
                PartnerId = context.Message.PartnerId,
                PromoCodeId = context.Message.PromoCodeId,
                PromoCode = context.Message.PromoCode,
                PreferenceId = context.Message.PreferenceId,
                BeginDate = context.Message.BeginDate,
                EndDate = context.Message.EndDate
            };

            var result = await _helperService.GivePromoCodesToCustomersWithPreferenceAsync(request);
        }
    }

    public class IPromocodeCreatedConsumerDefinition : ConsumerDefinition<IPromocodeCreatedConsumer>
    {
        public IPromocodeCreatedConsumerDefinition()
        {
            EndpointName = MassTransitConstans.IPromocodeCreatedEndpoint;
        }

        protected override void ConfigureConsumer(IReceiveEndpointConfigurator endpointConfigurator, IConsumerConfigurator<IPromocodeCreatedConsumer> consumerConfigurator)
        {
            endpointConfigurator.UseRetry(x => x.Intervals(100, 500, 1000));
        }
    }
}   
