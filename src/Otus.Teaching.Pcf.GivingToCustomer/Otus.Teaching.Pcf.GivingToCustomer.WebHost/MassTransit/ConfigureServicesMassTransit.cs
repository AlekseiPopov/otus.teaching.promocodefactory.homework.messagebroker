using System;
using MassTransit;
using MassTransit.Definition;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using MassTransit.Contracts;
using MassTransit.Consumers;

namespace Otus.Teaching.Pcf.ReceivingFromPartner.WebHost.MassTransit
{
    /// <summary>
    /// MassTransit configurations for ASP.NET Core
    /// </summary>
    public class ConfigureServicesMassTransit
    {
        public static void ConfigureServices(IServiceCollection services, IConfiguration Configuration)
        {
            var massTransitSection = Configuration.GetSection("MassTransit");
            var url = massTransitSection.GetValue<string>("Url");
            var host = massTransitSection.GetValue<string>("Host");
            var userName = massTransitSection.GetValue<string>("UserName");
            var password = massTransitSection.GetValue<string>("Password");
            if (massTransitSection == null || url == null || host == null)
            {
                throw new ArgumentNullException("Section 'mass-transit' configuration settings are not found in appSettings.json");
            }

            services.AddMassTransit(x =>
            {
                x.AddBus(busFactory =>
                {
                    var bus = Bus.Factory.CreateUsingRabbitMq(cfg =>

                    {
                        cfg.Host($"rabbitmq://{url}/{host}", configurator =>
                        {
                            configurator.Username(userName);
                            configurator.Password(password);
                        });

                        cfg.ConfigureEndpoints(busFactory, SnakeCaseEndpointNameFormatter.Instance);
                        cfg.UseJsonSerializer();
                        cfg.UseHealthCheck(busFactory);
                    });
                    
                    return bus;
                });

                x.AddConsumer<IPromocodeCreatedConsumer, IPromocodeCreatedConsumerDefinition>();
            });

            services.AddMassTransitHostedService();
        }
    }
}


