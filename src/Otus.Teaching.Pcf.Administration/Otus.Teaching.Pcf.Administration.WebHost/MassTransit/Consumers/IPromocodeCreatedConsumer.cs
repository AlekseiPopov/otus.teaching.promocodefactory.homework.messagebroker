﻿using System.Threading.Tasks;
using GreenPipes;
using MassTransit.ConsumeConfigurators;
using MassTransit.Contracts;
using MassTransit.Definition;
using Otus.Teaching.Pcf.Administration.WebHost.MassTransit.Constans;
using Otus.Teaching.Pcf.Administration.WebHost.Services;

namespace MassTransit.Consumers
{
    public class IPromocodeCreatedConsumer : IConsumer<IPromocodeCreated>
    {
        private readonly IHelperService _helperService;

        public IPromocodeCreatedConsumer(IHelperService helperService)
        {
            _helperService = helperService;
        }
        public async Task Consume(ConsumeContext<IPromocodeCreated> context)
        {
            var result = await _helperService.UpdateAppliedPromocodesAsync(context.Message.PartnerId);
        }
    }

    public class IPromocodeCreatedConsumerDefinition : ConsumerDefinition<IPromocodeCreatedConsumer>
    {
        public IPromocodeCreatedConsumerDefinition()
        {
            EndpointName = MassTransitConstans.IPromocodeCreatedEndpoint;
        }

        protected override void ConfigureConsumer(IReceiveEndpointConfigurator endpointConfigurator, IConsumerConfigurator<IPromocodeCreatedConsumer> consumerConfigurator)
        {
            endpointConfigurator.UseRetry(x => x.Intervals(100,500,1000));
        }
    }
}
