﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.Pcf.Administration.WebHost.Models;

namespace Otus.Teaching.Pcf.Administration.WebHost.Services
{
    public interface IHelperService
    {
        public Task<bool> UpdateAppliedPromocodesAsync(Guid id);
    }
}
